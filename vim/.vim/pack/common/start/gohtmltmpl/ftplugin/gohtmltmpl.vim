" From : https://github.com/fatih/vim-go/blob/master/ftplugin/gohtmltmpl.vim
if exists("b:did_ftplugin")
  finish
endif

runtime! ftplugin/html.vim

" vim: sw=2 ts=2 et
