dotfiles
========

Personal configuration for

- alacritty
- bash
- fuzzel
- gammastep
- git
- gtk
- mako
- mpd + ncmpcpp
- mpv
- Pcmanfm-QT
- QT 5 and 6
- ripgrep
- sway
- vim
- waybar

and maybe other things, managed with GNU Stow (using --target=${HOME})

_Note_ : I use Grammalecte in Vim. You will have to install it manually, or remove the plugin from the `.vim/pack/common/start/vim-Grammalecte/` folder.
See [Grammalecte Vim plugin repo](https://github.com/dpelle/vim-Grammalecte) and
[Grammalecte website (French)](https://grammalecte.net/) or
[Grammalecte Archlinux package](https://archlinux.org/packages/community/any/grammalecte/)
for more information.

[More about GNU Stow](http://brandon.invergo.net/news/2012-05-26-using-gnu-stow-to-manage-your-dotfiles.html)

## Root PS1

```
PS1="\n\[\e[38;5;232m\]\[\e[1;48;5;9m\]  \u \[\e[00m\]\[\e[38;5;232m\]\[\e[1;48;5;32m\]   \h \[\e[38;5;232m\]\[\e[1;48;5;11m\]   \w \[\e[00m\] \$(__cmd_err_code)\n» "
```

## /etc/issue

put this inside :

```
 \e[H\e[2J
           \e[0;36m.
          \e[0;36m/ \\       \e[1;37m     _             _         
         \e[0;36m/   \\      \e[1;37m    / \\   _ __ ___| |__    \e[1;36m| *
        \e[0;36m/^.   \\     \e[1;37m   / _ \\ | '__/ __| '_ \\   \e[1;36m| | |-^-. |   | \\ /
       \e[0;36m/  .-.  \\    \e[1;37m  / ___ \\| | | (__| | | \\  \e[1;36m| | |   | |   |  X
      \e[0;36m/  (   ) _\\   \e[1;37m /_/   \\_\\_|  \\___|_| |_|  \e[1;36m| | |   | ^._.| / \\ \e[0;37mTM
     \e[1;36m/ _.~   ~._^\\
    \e[1;36m/.^         ^.\\ \e[0;37mTM


Arch Linux \r (\l)
Welcome to \n
\d \t

```

## Contributing

Found a bug ? Have an idea for a config or a script ? Feel free to [send a patch by email](https://git-send-email.io/).
