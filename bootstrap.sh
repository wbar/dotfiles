#!/bin/env bash
set -euo pipefail

readonly HOME_BIN="${HOME}/bin"

test_local_bin() {
    [ ! -L "${HOME_BIN}/$1" ] && echo "TODO : create symlink for $1" || echo "[OK] Link to $1 exists"
}

# .config dir
if [ ! -d ~/.config ]; then
    mkdir ~/.config
fi

# ---
# configs
stow -t "${HOME}" alacritty
stow -t "${HOME}" backup
stow -t "${HOME}" bash
stow -t "${HOME}" fuzzel
stow -t "${HOME}" gammastep
stow -t "${HOME}" git
stow -t "${HOME}" gitcheck
stow -t "${HOME}" gtk
stow -t "${HOME}" qt5ct
stow -t "${HOME}" qt6ct
stow -t "${HOME}" mako
stow -t "${HOME}" mpd
stow -t "${HOME}" mpv
stow -t "${HOME}" ncmpcpp
stow -t "${HOME}" pcmanfm-qt
stow -t "${HOME}" ripgrep
stow -t "${HOME}" sway
stow -t "${HOME}" vim
stow -t "${HOME}" waybar

# ---
# scripts and tools
stow -t "${HOME}" utils

# getting a little bit too far but ...
test_local_bin "compta"
test_local_bin "fichespaie"

echo "Done."

