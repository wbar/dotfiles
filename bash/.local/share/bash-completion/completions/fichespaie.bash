#/usr/bin/env bash
_fichespaie_completions()
{
  if [ "${#COMP_WORDS[@]}" != "2" ]; then
    return
  fi

  COMPREPLY=($(compgen -W "open close" -- "${COMP_WORDS[1]}"))
}

complete -F _fichespaie_completions fichespaie
